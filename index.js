const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const AssetsPlugin = require('assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const packageJSON = require('./package.json');

const getConfig = function(baseDir, paths) {
  const config = {
    entry: Object.assign({
      index: path.join(baseDir, 'src/pages/index'),
    }, paths),
    output: {
      path: path.join(baseDir, '../project/assets/js'),
      filename: '[name].js',
    },
    devtool: 'cheap-module-eval-source-map',
    module: {
      rules: [
        {
          use: ['babel-loader'],
          test: /\.js$/,
          include: [
            path.join(baseDir, 'src/assets/js'),
            path.join(baseDir, 'src/components'),
            path.join(baseDir, 'src/pages'),
          ],
          exclude: [
            path.join(baseDir, 'src/assets/js/vendor'),
            path.join(baseDir, 'node_modules'),
          ],
        },
      ],
    },
    resolve: {
      modules: [
        path.join(baseDir, 'src/assets/js'),
        path.join(baseDir, 'src/components'),
        path.join(baseDir, 'src/pages'),
        path.join(baseDir, 'node_modules'),
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        BRAND: JSON.stringify(packageJSON.name),
      }),
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: false,
        mangle: true,
      }),

      new CleanWebpackPlugin(['js'], {
        root: path.join(baseDir, '../project/assets'),
        dry: false,
        exclude: ['vendor'],
      }),

      new AssetsPlugin({
        filename: 'rev-manifest.json',
        path: path.join(baseDir, '../project/assets/json'),
        processOutput: (assets) => {
          const currentManifest = JSON.parse(fs.readFileSync(path.join(baseDir, '../project/assets/json/rev-manifest.json'), 'utf-8'));
          const newData = {};
          Object.keys(assets).forEach((key) => {
            newData[`${key}.js`] = assets[key].js;
          });

          const newManifest = Object.assign(currentManifest, newData);

          return JSON.stringify(newManifest);
        },
      }),
    ],
    externals: {
      jquery: 'jQuery',
      jQuery: 'jQuery',
      $: 'jQuery',
    },
  }

  if (process.env.NODE_ENV === 'production') {
    config.output.filename = '[name]-[chunkhash].js';
    config.devtool = false;
  }

  return config;
};

module.exports = getConfig;
